resource "google_container_cluster" "prod" {
  name     = "dataans-gke-prod"
  location = "europe-central2"
  
  remove_default_node_pool = true
  initial_node_count       = 1

  network    = google_compute_network.vpc.name
  subnetwork = google_compute_subnetwork.subnet.name

  logging_service = "none"
  monitoring_service = "none"

  release_channel {
    channel = "RAPID"
  }

  addons_config {
    horizontal_pod_autoscaling {
      disabled = true
    }

    http_load_balancing {
      disabled = true
    }
  }


  vertical_pod_autoscaling {
    enabled = false
  }
}

resource "google_container_node_pool" "main_pool" {
  name       = "${google_container_cluster.prod.name}-node-pool"
  location   = "europe-central2"
  cluster    = google_container_cluster.prod.name
  node_count = 1

  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/monitoring.write",
      "https://www.googleapis.com/auth/devstorage.read_write",
      "https://www.googleapis.com/auth/datastore"
    ]

    labels = {
      env = "dataans"
    }

    preemptible  = true
    machine_type = "e2-small"
    tags         = ["dataans-prod-gke"]
    disk_size_gb = 10
    image_type = "cos_containerd"
    disk_type = "pd-standard"
    metadata = {
      disable-legacy-endpoints = "true"
    }
  }

  autoscaling {
    max_node_count = 1
    min_node_count = 1
  }
}

