provider "google" {
  credentials = file("dataans-8d615f8aea64.json")
  project = "dataans"
  region = "europe-central2"
}

data "google_iam_policy" "public_access" {
  binding {
    role = "roles/storage.objectViewer"
    members = [
      "allUsers",
    ] 
  }
}

resource "google_storage_bucket_iam_policy" "public_access_policy" {
  bucket = google_storage_bucket.gcs_dataans_user_profile.name
  policy_data = data.google_iam_policy.public_access.policy_data
}

resource "google_storage_bucket" "gcs_dataans_user_profile" {
  name = "avatars.dataans.com"
  project = "dataans"
  location = "europe-central2"
  force_destroy = true
}
