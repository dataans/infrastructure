resource "google_compute_network" "vpc" {
  name                    = "dataans-vpc"
  auto_create_subnetworks = "false"
}

resource "google_compute_subnetwork" "subnet" {
  name          = "dataans-main-subnet"
  region        = "europe-central2"
  network       = google_compute_network.vpc.name
  ip_cidr_range = "10.10.0.0/24"
}
