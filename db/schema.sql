create extension if not exists "uuid-ossp";

create table users (
    id uuid primary key,
    username varchar(256) not null unique,
    full_name varchar(512) not null,
    avatar_url varchar(256) not null,
    joined_at timestamp with time zone not null default now(),
    two_fa_secret varchar(64),
    is_2fa_enabled bool not null default false
);

create table emails (
    id uuid primary key,
    email varchar(256) not null unique,
    user_id uuid references users(id),
    added_at timestamp with time zone not null default now()
);

alter table users add column primary_email varchar(256) not null;

alter table users add constraint primary_email_in_emails_fkey foreign key (primary_email) references emails(email) deferrable;

create table passwords (
    id uuid primary key references users(id),
    nonce varchar(48) not null,
    hash varchar(264) not null,
    version smallint default 1
);

create table _2fa_recovery_codes (
    id uuid primary key,
    nonce varchar(48) not null,
    hash varchar(264) not null,
    version smallint default 1,
    user_id uuid references users(id)
);

create table questions (
    pk uuid primary key,
    id integer unique not null,
    title varchar(256) not null,
    text varchar(2048) not null,
    created_at timestamp not null default now(),
    updated_at timestamp not null default now(),
    creator_id uuid not null references users(id),
    up_votes integer not null default 0,
    views integer not null default 0
);

create table samples (
    id uuid primary key,
    query varchar(4096) not null,
    sample_schema varchar(4096) not null,
    last_execution timestamp with time zone not null default now()
);

create table answers (
    id uuid primary key,
    text varchar(2048) not null,
    created_at timestamp with time zone default now(),
    updated_at timestamp with time zone default now(),
    creator_id uuid not null references users(id),
    sample_id uuid
);

create table comments (
    id uuid primary key,
    text varchar(1024) not null,
    created_at timestamp with time zone default now(),
    updated_at timestamp with time zone default now(),
    creator_id uuid not null references users(id),
    sample_id uuid
);

create table answers_comments (
    answer_id uuid not null references answers(id),
    comment_id uuid not null references comments(id),
    primary key (answer_id, comment_id)
);

create table questions_answers (
    question_id uuid not null references questions(pk),
    answer_id uuid not null references answers (id),
    primary key (question_id, answer_id)
);

create table answers_votes (
    answer_id uuid not null references answers(id),
    user_id uuid not null references users(id),
    value smallint not null,
    primary key (answer_id, user_id)
);

